#!/usr/bin/env bash
# -*- mode: bash-script -*-

#==============================================================================
#
# FILE:         build.sh
# DESCRIPTION:  Script to build YACReaderLibraryServer container via docker.
# REQUIREMENTS: bash >= 3.0
# AUTHOR:       Fahmi Salleh
# VERSION:      1.4.3
# REPOSITORY:   https://gitlab.com/Fahmi.Salleh/yacreaderlibraryserver
# LICENSE:
#               Copyright (c) 2018 - 2022 Fahmi Salleh, All Rights Reserved.
#
#               Permission is hereby granted, free of charge, to any person
#               obtaining a copy of this software and associated documentation
#               files (the "Software"), to deal in the Software without
#               restriction, including without limitation the rights to use,
#               copy, modify, merge, publish, distribute, sublicense, and/or
#               sell copies of the Software, and to permit persons to whom the
#               Software is furnished to do so, subject to the following
#               conditions:
#
#               The above copyright notice and this permission notice shall be
#               included in all copies or substantial portions of the Software.
#               The name of Fahmi Salleh not be used in advertising or publicity
#               pertaining to distribution of the software without specific,
#               written prior permission.
#
#               FAHMI SALLEH DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
#               SOFTWARE. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY
#               OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
#               THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
#               PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
#               COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#               LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
#               ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
#               USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#==============================================================================

SECONDS=0
VOLUME_BASE="./volume-base"
DIR_DATA_DEFAULT="${VOLUME_BASE}/default/data"
DIR_COMICS_DEFAULT="${VOLUME_BASE}/default/comics"
DIR_DATA_CUSTOME="${VOLUME_BASE}/custome/data"
DIR_COMICS_CUSTOME="${VOLUME_BASE}/custome/comics"

if [[ -f "target-version" ]]
then
    # shellcheck disable=SC1091
    source target-version

    if [[ -z "${BUILD_VER}" ]] && [[ -z "${OS_VER}" ]] && [[ -z "${YACREADER_VER}" ]] && [[ -z "${UNARR_VER}" ]]
    then
        echo "ERROR: Required version not set on varibles BUILD_VER, OS_VER, YACREADER_VER, and UNARR_VER in file 'target-version'!"
        exit 2
    else
        [[ -d ${VOLUME_BASE} ]] || mkdir -p ${DIR_DATA_DEFAULT} ${DIR_COMICS_DEFAULT} ${DIR_DATA_CUSTOME} ${DIR_COMICS_CUSTOME}

        IMAGE_RELEASE_TAG="${YACREADER_VER}.${BUILD_VER}"
    fi
else
    echo "ERROR: File target-version not found!"
    exit 1
fi

echo " "
echo "START:"

# Build YACReaderLibraryServer container image
# Eg: docker build -t sirdocker/yacreaderlibraryserver:9.12.0 -t sirdocker/yacreaderlibraryserver:latest .
echo " "
echo "IMAGE_BUILD: PLATFORM=linux/amd64, OS_NAME=alphine, OS_VER=${OS_VER}, YACREADER_VER=${YACREADER_VER}, UNARR_VER=${UNARR_VER}, NAME=sirdocker/yacreaderlibraryserver TAGS=${YACREADER_VER},latest"
BUILD_START_SEC=${SECONDS}
docker buildx build \
-t sirdocker/yacreaderlibraryserver:"${IMAGE_RELEASE_TAG}" \
-t sirdocker/yacreaderlibraryserver:latest \
--platform linux/amd64 \
--build-arg OS_VER="${OS_VER}" \
--build-arg YACREADER_VER="${YACREADER_VER}" \
--build-arg UNARR_VER="${UNARR_VER}" \
--no-cache .
BUILD_STOP_SEC=${SECONDS}

echo " "
echo "IMAGE_LIST: "
docker image list sirdocker/yacreaderlibraryserver

IMAGE_ID=$(docker image list --quiet sirdocker/yacreaderlibraryserver:"${IMAGE_RELEASE_TAG}")

# Run YACReaderLibraryServer with default library name: ComicShop
# Eg: docker run -d -p 8080:8080 -v /Users/fahmi/Workspaces/Projects/YACReaderStore/Data:/data -v /Users/fahmi/Workspaces/Projects/YACReaderStore/Comics:/comics --name=comic-service-01 sirdocker/yacreaderlibraryserver:9.12.0
echo " "
echo "CONTAINER_RUN: with default library..."
CONTAINER_DEFAULT_ID=$(docker run --platform linux/amd64 --rm -d -p 8081:8080 -v "${DIR_DATA_DEFAULT}":/data -v "${DIR_COMICS_DEFAULT}":/comics --name=comic-server-default "${IMAGE_ID}")
echo "${CONTAINER_DEFAULT_ID}"

sleep 15

# Run YACReaderLibraryServer with specific library name.
# Eg: docker run -d -p 8080:8080 -v /Users/fahmi/Workspaces/Projects/YACReaderStore/Data:/data -v /Users/fahmi/Workspaces/Projects/YACReaderStore/Comics:/comics -e LIBNAME=ComicGalaxy --name=comic-service-01 sirdocker/yacreaderlibraryserver:9.12.0
echo " "
echo "CONTAINER_RUN: with custome library..."
CONTAINER_CUSTOME_ID=$(docker run --platform linux/amd64 --rm -d -p 8082:8080 -v "${DIR_DATA_CUSTOME}":/data -v "${DIR_COMICS_CUSTOME}":/comics -e LIBNAME=ComicLibrary --name=comic-server-custome "${IMAGE_ID}")
echo "${CONTAINER_CUSTOME_ID}"

sleep 15

echo " "
echo "CONTAINER_LIST: "
docker container list --last -1 -s

echo " "
echo "CONTAINER_TEST: default..."

# Check version
TEST_GET_VER=$(docker exec "${CONTAINER_DEFAULT_ID}" YACReaderLibraryServer --version | awk '{print $2}')
[[ "${TEST_GET_VER}" == "${YACREADER_VER}" ]] && RESULT="PASSED" || RESULT="FAILED"
echo "[${RESULT}] CHECK_VERSION: ${TEST_GET_VER}"

# Check process cron
TEST_CHECK_CRON=$(docker exec "${CONTAINER_DEFAULT_ID}" ps aux | grep crond | grep -v grep)
[[ -n "${TEST_CHECK_CRON}" ]] && RESULT="PASSED" || RESULT="FAILED"
echo "[${RESULT}] CHECK_PROCESS: chron"

# Check process YACReaderLibraryServer
TEST_CHECK_YRL=$(docker exec "${CONTAINER_DEFAULT_ID}" ps aux | grep YACReaderLibraryServer | grep -v grep)
[[ -n "${TEST_CHECK_YRL}" ]] && RESULT="PASSED" || RESULT="FAILED"
echo "[${RESULT}] CHECK_PROCESS: YACReaderLibraryServer"

# Check file config
TEST_CHECK_CONF=$(docker exec "${CONTAINER_DEFAULT_ID}" ls -l /data | grep YACReaderLibrary.ini | grep -v grep)
[[ -n "${TEST_CHECK_CONF}" ]] && RESULT="PASSED" || RESULT="FAILED"
echo "[${RESULT}] CHECK_FILE: YACReaderLibrary.ini"

# Check file logs
TEST_CHECK_LOG=$(docker exec "${CONTAINER_DEFAULT_ID}" ls -l /data | grep yacreaderlibrary.log | grep -v grep)
[[ -n "${TEST_CHECK_LOG}" ]] && RESULT="PASSED" || RESULT="FAILED"
echo "[${RESULT}] CHECK_FILE: yacreaderlibrary.log"

# Check Default Library
TEST_GET_LIB=$(docker exec "${CONTAINER_DEFAULT_ID}" YACReaderLibraryServer list-libraries | awk '/ComicShop/ {print $1}')
[[ "${TEST_GET_LIB}" == "ComicShop" ]] && RESULT="PASSED" || RESULT="FAILED"
echo "[${RESULT}] CHECK_LIBRARY: ComicShop"

# Test Create Library
docker exec "${CONTAINER_DEFAULT_ID}" mkdir /tmp/comics-shelf
TEST_CREATE_LIB=$(docker exec "${CONTAINER_DEFAULT_ID}" YACReaderLibraryServer create-library ComixGalaxy /tmp/comics-shelf)
[[ "${TEST_CREATE_LIB}" == "Processing comicsDone!" ]] && RESULT="PASSED" || RESULT="FAILED"
echo "[${RESULT}] CREATE_LIBRARY: ComixGalaxy"

# Check Library Count
TEST_COUNT_LIB=$(docker exec "${CONTAINER_DEFAULT_ID}" YACReaderLibraryServer list-libraries | wc -l | tr -d ' ')
[[ ${TEST_COUNT_LIB} -eq 2 ]] && RESULT="PASSED" || RESULT="FAILED"
echo "[${RESULT}] COUNT_LIBRARY: ${TEST_COUNT_LIB}"
[[ ! ${TEST_COUNT_LIB} -eq 2 ]] && docker exec "${CONTAINER_DEFAULT_ID}" YACReaderLibraryServer list-libraries

# Test Remove Library
TEST_REMOVE_LIB=$(docker exec "${CONTAINER_DEFAULT_ID}" YACReaderLibraryServer remove-library ComixGalaxy | awk '/Library removed/ {print $NF}')
[[ "${TEST_REMOVE_LIB}" == "ComixGalaxy" ]] && RESULT="PASSED" || RESULT="FAILED"
echo "[${RESULT}] REMOVE_LIBRARY: ${TEST_REMOVE_LIB}"


if [[ "${1}" == "-p" ]]; then 
    echo " "
    read -r -p "Do manual image inspection! Press [Enter] key to resume ..."
fi

echo " "
echo "CONTAINER_STOP: with default library..."
docker container stop "${CONTAINER_DEFAULT_ID}"

echo " "
echo "CONTAINER_STOP: with custome library..."
docker container stop "${CONTAINER_CUSTOME_ID}"

echo " "
echo "CONTAINER_LIST: "
docker container list --last -1 -s

[[ -d ${VOLUME_BASE} ]] && rm -rf ${VOLUME_BASE}

RUNTIME_BUILD=$(((BUILD_STOP_SEC - BUILD_START_SEC) / 60))
RUNTIME_SCRIPT=$((SECONDS / 60))
echo " "
echo "FINISHED: build runtime ${RUNTIME_SCRIPT} minutes, script runtime ${RUNTIME_BUILD} minutes"

TRIVY_IMAGE_TAG="0.49.1"
SCAN_IMAGE_TAG="${IMAGE_RELEASE_TAG}"
SCAN_LOG_FILE="scan_yacrls_${IMAGE_RELEASE_TAG}_$(date '+%Y-%m-%d_%H-%M-%S').log"

echo " "
echo "START:"

echo " "
echo "SECURITY_SCANNER : Trivy ${TRIVY_IMAGE_TAG} security scanner for vulnerability"
docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -v "${HOME}/Library/Caches:/root/.cache/" "aquasec/trivy:${TRIVY_IMAGE_TAG}" image --format table -o "/root/.cache/${SCAN_LOG_FILE}" "sirdocker/yacreaderlibraryserver:${SCAN_IMAGE_TAG}" > /dev/null 2>&1
[[ -f "${HOME}/Library/Caches/${SCAN_LOG_FILE}" ]] && cp "${HOME}/Library/Caches/${SCAN_LOG_FILE}" . || echo "Log file ${HOME}/Library/Caches/${SCAN_LOG_FILE} not found!"
[[ -f "./${SCAN_LOG_FILE}" ]] && cat "./${SCAN_LOG_FILE}" || echo "Log file ./${SCAN_LOG_FILE} not copied!"

echo "FINISHED: security scan log file ${SCAN_LOG_FILE}"