 <img align="right" width="100" height="100" src="https://static.macupdate.com/products/52821/m/yacreader-logo.png?v=1598343254">

# YACReaderLibraryServer Container

[YACReader](https://www.yacreader.com) "*Yet another comic reader*" is an app for reading and managing digital comics, the whole ecosystem is formed by YACReader and [YACReaderLibrary for desktops](https://www.yacreader.com/downloads), YACReaderLibraryServer for embeded devices, like a NAS, and [YACReader for iOS](https://apps.apple.com/app/id635717885). YACReader  or "*Yet Another Comic Reader*" is developed by Luis Ángel San Martín Rodríguez (luisangelsm@gmail.com) under GPL v3 license.

This YACReaderLibraryServer is the headless (no GUI) version of YACReaderLibrary in a container. Everybody can quickly run a private comics server at home. Your family can browse and read comics using the YACReader client mobile on iPhone or iPad.

YACReaderLibraryServer container features:

* Small image size, **64MB compressed**, and **165MB uncompressed**.
* Start the container. If no library is detected, it will automatically create, scan, and add new comics.
* In case of the container upgrade, just map the existing data and comics directory on the host to a new container, the existing library will automatically be added.
* Automatically scan newly added comics. The scan runs every 3 minutes.

For any improvement suggestions or fixes, feel free to contact me by email (you can get an my email address from the image LABEL email).

**ATTENTION!** YACReaderLibraryServer does not have an authentication mechanism! It is advisable to run the YACReaderLibraryServer container inside home LAN only.

**DISLAIMER:** This containerised YACReaderLibraryServer doesn't have any connection or is affiliated with the official [YACReader](https://www.yacreader.com). I'm one of the iOS YACReader fans. I built a YACReaderLibraryServer container to quickly deploy own comic service on my Synology NAS.

**NOTE:** As part of security enhancement and best practice adoption of dockerized application, start with `sirdocker/yacreaderlibraryserver:9.14.1.0` image, application run as non-root users (`yacuser:yacgroup`). If you have a volume mount /data and /comics permission Issue, please see the troubleshooting section below.

### Release Tags:

* Latest:
  * [9.14.1.0](https://hub.docker.com/layers/sirdocker/yacreaderlibraryserver/9.14.1.0/images/sha256-e3600059b9c7a1197e24a74ec8b99bcf22bb42276bc5b3e6ed6d4b324b301d5f?context=repo)

* Previous -1:
  * [9.13.1.0](https://hub.docker.com/layers/sirdocker/yacreaderlibraryserver/9.13.1.0/images/sha256-bcbeec8ca678b181074ffcfbe31f4f1027265287472316205a2fdaf5678d0f64?context=explore)

* Previous -2:
  * [9.12.0.0](https://hub.docker.com/layers/sirdocker/yacreaderlibraryserver/9.12.0.0/images/sha256-43b44c2450aafa402db6b1e319c24a73ca0d56c9e5498bc84376e4e86f37fc13?context=explore)

* Previous -3:
  * [9.8.2.0](https://hub.docker.com/layers/sirdocker/yacreaderlibraryserver/9.8.2/images/sha256-b03b801b52738c1b8abf0dcf852919574e540cdcbe261e4eec6ea063117f616a?context=explore)


## Quick Setup

#### Get the Image

Pull image from [sirdocker/yacreaderlibraryserver](https://hub.docker.com/repository/docker/sirdocker/yacreaderlibraryserver/general) Docker Hub. You can refer on **Release Tags** section above for available tags.

To fetch latest image:

```shell
docker pull sirdocker/yacreaderlibraryserver
```

To get specific release:
```shell
docker pull sirdocker/yacreaderlibraryserver:9.14.1.0
```

#### Run the Container:

Start the container using the following command:

```shell
docker run -d -p <port>:8080 -v <data_dir>:/data -v <comics_dir>:/comics --name=<container_name> sirdocker/yacreaderlibraryserver:latest

eg: docker run -d -p 8080:8080 -v /store/yacreader/data:/data -v /store/yacreader/comics:/comics --name=comics-service-01 sirdocker/yacreaderlibraryserver:9.14.1.0
```

`<container_name>` to give your container name   
`<port>` is port to assign on the host.   
`<data_dir>` is a directory on the host where config and a log file will be saved   
`<comics_dir>` is a directory on the host where all comic files are located   

The library named **ComicShop** will be created by default. You can change it by adding the following argument: 

```
-e LIBNAME=<new_library_name> 

eg: docker run -d -p 8080:8080 -v /store/yacreader/data:/data -v /store/yacreader/comics:/comics -e LIBNAME=ComicGalaxy --name=comics-service-01 sirdocker/yacreaderlibraryserver:latest
```

## Accessing Library

Access your YACReaderLibraryServer by pointing your YACReader app or browser to: `http://<hostname>:<port>`

`<hostname>` container hostname or ip address   
`<port>` port for client to connect   

## Optional Command

#### Create Library:  
```shell
docker exec yacreaderlibraryserver YACReaderLibraryServer create-library <library-name> <comic_directory>
```
`<library_name>` your comic library name to create.
`<comic_directory>` path to the directory where all comics. Since running using the yacuser (non-root), the comics directory must be accessible by yacuser.

#### Update Library - when you've added new comics:  
```shell
docker exec yacreaderlibraryserver YACReaderLibraryServer update-library /comics
```

#### List All Libraries
```shell
docker exec YACReaderLibraryServer YACReaderLibraryServer list-libraries
```

#### Remove Library
```shell
docker exec YACReaderLibraryServer YACReaderLibraryServer remove-library <library-name>
```

`<library_name>` your comic library name to delete. 

## Troubleshooting

### Volume Mount `/data` and `/comics` Permission Issue

Some of you upgraded from the previous image (run as root) might have permission issues on the `/data` and `/comics` volume mount. You can try the below steps to correct permission and ownership issues:

1. Get the previous image `sirdocker/yacreaderlibraryserver:9.13.1.0`, which runs as a root, and run it as a `comic-service-x` container.
   ```shell
   docker run --platform linux/amd64 -d -p 8080:8080 -v <existing_data_directory>:/data -v <existing_comics_directory>:/comics --name=comic-service-x sirdocker/yacreaderlibraryserver:9.13.1.0
   ```
2. Add group `yacgroup`.
   ```shell
   docker exec comic-service-x addgroup yacgroup 
   ```
3. Add user `yacuser`.
   ```shell
   docker exec comic-service-x adduser -S -G yacgroup yacuser
   ```
4. Change ownership of the directory of `/data` and `/comics` recursively. 
   ```shell
   docker exec comic-service-x chown -R yacuser:yacgroup /data /comics
   ```
5. Update permission of directory `/data` and `/comics`.
   ```shell
   docker exec comic-service-x chmod 755 /data /comics
   ```
6. Stop and remove the container.
   ```shell
   docker stop comic-service-x && docker rm $_
   ```

## Built With
* [Alpine Linux](https://alpinelinux.org) - A minimal Linux image
* [Docker Desktop on Mac](https://www.docker.com/products/docker-desktop) - tool to containerize applications on your desktop
* [Bash](https://www.gnu.org/software/bash/) - Scripting language used
* [ShellCheck](https://www.shellcheck.net/) - Shell linter (Static Analysis)
* [Visual Studio Code](https://code.visualstudio.com/) - Editor for scripting
* [vscode-shellcheck](https://github.com/timonwong/vscode-shellcheck) - An extension to use shellcheck in vscode
* [Trivy](https://trivy.dev) - Open source security scanner