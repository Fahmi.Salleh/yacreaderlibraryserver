# OS Image variables for YACReaderLibraryServer
ARG OS_VER

# Build Image
FROM alpine:$OS_VER AS build

# Build variables for YACReaderLibraryServer
ARG YACREADER_VER
ARG UNARR_VER
ARG INSTALL_DIR="/opt/yacreaderlibraryserver"

# Working directory for build
WORKDIR /workdir

# Update OS and install required packages for build
RUN apk add --update --no-cache \
    build-base \
    cmake \
    git \
    qt5-qtbase-dev \
    poppler-qt5-dev

# Download YACReader source code
RUN git clone https://github.com/YACReader/yacreader.git . && \
    git checkout $YACREADER_VER

# Download unarr
RUN git clone https://github.com/selmf/unarr.git

# Download and include unarr libraries for build
RUN cd unarr && \
    git checkout $UNARR_VER && \
    mkdir build && \
    cd build && \
    cmake .. -DBUILD_SHARED_LIBS=ON -DENABLE_7Z=ON && \
    make && \
    make install

# Compile headless version of YACReaderLibraryServer
RUN cd /workdir/YACReaderLibraryServer && \
    qmake-qt5 PREFIX=$INSTALL_DIR "CONFIG+=unarr server_standalone" YACReaderLibraryServer.pro && \
    make && \
    make install

# Copy setup support and  config file with the settings relevant for `YACReaderLibraryServer`
ADD YACReaderLibrary.ini $INSTALL_DIR/etc/
ADD YACReaderLibraryService $INSTALL_DIR/bin/
RUN chmod u+x $INSTALL_DIR/bin/YACReaderLibraryService


# Deploy Image
FROM alpine:$OS_VER

# This is me :-)
LABEL maintainer="Fahmi Salleh"
LABEL email="fahmie@gmail.com"

# Build variables for YACReaderLibraryServer
ARG INSTALL_DIR="/opt/yacreaderlibraryserver"

# Update OS and install required packages for YACReaderLibraryServer
RUN apk add --update --no-cache \
    qt5-qtbase \
    qt5-qtbase-sqlite \
    poppler-qt5

# create and set non-root USER
RUN addgroup yacgroup && \
    adduser -S -G yacgroup yacuser

# Copy YACReaderLibraryServer compile previously
COPY --from=build /usr/local /usr/local
COPY --from=build $INSTALL_DIR/ $INSTALL_DIR/

# Create symbolic link for config and log files
RUN mkdir -p /data /comics /home/yacuser/.local/share/YACReader && \
    ln -s /data /home/yacuser/.local/share/YACReader/YACReaderLibrary

# Set ownership/permission of application to yacuser
RUN chown -R yacuser:yacgroup /data /comics $INSTALL_DIR && \
    chmod 755 /data /comics $INSTALL_DIR

# Schedule comics scan for libraries every 3 minutes
RUN echo "*/3     *       *       *       *       YACReaderLibraryService update" >> /var/spool/cron/crontabs/root

# Non-root user to run the server
USER yacuser

# Set required envrionment variables for YACReaderLibraryServer
ENV PATH="$PATH:$INSTALL_DIR/bin"
ENV LC_ALL="en_US.UTF-8"

# Add specific volumes:
# * /data - location for config and logs
# * /comics - location for comics
VOLUME /data
VOLUME /comics

# Port to expose
EXPOSE 8080/tcp

# Default starting point
ENTRYPOINT ["YACReaderLibraryService"]

# Check service
# HEALTHCHECK --interval=15s --timeout=10s --start-period=30s \
#     CMD wget --no-verbose --tries=1 --spider http://localhost:8080/ || exit 1