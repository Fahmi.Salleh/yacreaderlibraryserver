# YACReaderLibraryServer Container Changelog

Version counting is based on semantic versioning *<YACReaderLibrary_Version>.<Container_Release>*

## Release 9.14.1.0

### YACReaderLibraryServer
* **YACReaderLibrary** upgraded to version [9.14.1](https://github.com/YACReader/yacreader/releases/tag/9.14.1).

* **YACReaderLibraryService** version 1.1.0.
* ***Unarr*** version [1.1.1](https://github.com/selmf/unarr/releases/tag/v1.1.1).

* YACReaderLibraryServer has its config section under [libraryConfig] in the config file `YACReaderLibrary.ini`. Setting includes:
  * Determines whether legacy metadata in XML format is parsed or not (and added to the database) when new comics are added.
  * Enables libraries to update at start-up.
  * Enables periodic library updates.
  * Enables scheduled updates at selected times.
* In the container above, the settings are not enabled by default as we still use crond for periodic libraries due to the lowest interval allowed by the built-in scheduler every 30 minutes being too long. The crond updates the library every 3 minutes interval.
* Now, YACReaderLibraryServer runs using non-root user yacuser for better security. So, the new library folder path only allows specific paths like `/opt/yacreaderlibraryserver` and `/home/yacuser`.

### Base Image
* Upgraded to base image **Alpine Linux** version [3.19.1](https://www.alpinelinux.org/posts/Alpine-3.19.1-released.html), a maintenance release of the 3.19 series.
* This release includes various bug fixes and security updates, including security fixes for OpenSSL:
  * [CVE-2023-6129](https://security.alpinelinux.org/vuln/CVE-2023-6129)
  * [CVE-2023-6237](https://security.alpinelinux.org/vuln/CVE-2023-6237)
  * [CVE-2024-0727](https://security.alpinelinux.org/vuln/CVE-2024-0727)

## Release 9.13.1.0

### YACReaderLibraryServer
* **YACReaderLibrary** upgraded to version [9.13.1](https://github.com/YACReader/yacreader/releases/tag/9.13.1).
* ***YACReaderLibraryService** version 1.0.0.*
* **Unarr** upgraded to version [1.1.1](https://github.com/selmf/unarr/releases/tag/v1.1.1).

### Base Image
* Upgraded to base image **Alpine Linux** version [3.19.0](https://www.alpinelinux.org/posts/Alpine-3.19.0-released.html).

## Release 9.12.0.0

### YACReaderLibraryServer
* **YACReaderLibrary** upgraded to version [9.12.0](https://github.com/YACReader/yacreader/releases/tag/9.12.0).
* ***YACReaderLibraryService** version 1.0.0.*
* ***Unarr** version [1.0.1](https://github.com/selmf/unarr/releases/tag/v1.0.1).*

### Base Image
* Upgraded to base image **Alpine Linux** version [3.18.2](https://www.alpinelinux.org/posts/Alpine-3.15.9-3.16.6-3.17.4-3.18.2-released.html).

## Release 9.8.2.0

### YACReaderLibraryServer
* **YACReaderLibrary** version [9.8.2](https://github.com/YACReader/yacreader/releases/tag/9.8.2).
* **YACReaderLibraryService** version 1.0.0.
* **Unarr** version [1.0.1](https://github.com/selmf/unarr/releases/tag/v1.0.1).

### Base Image
* **Alpine Linux** version [3.14.2](https://www.alpinelinux.org/posts/Alpine-3.14.2-released.html).